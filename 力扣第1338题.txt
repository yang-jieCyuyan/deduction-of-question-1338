class Solution {
public:
    int minSetSize(vector<int>& arr) {
        int ans=0;
        int n=arr.size();
        unordered_map<int,int> count;
        for(auto& num:arr)
           ++count[num];
        vector<int> v;
        for(auto& x:count)
           v.push_back(x.second);
        sort(v.begin(),v.end(),greater());
        int j=count.size();
        int sum=0;
        for(int i=0;i<j;i++)
        {
            sum+=v[i];
        }
        sum=sum%2==0?sum/2:sum/2+1;
        int x=0;
        for(int i=0;i<j;i++)
        {
            ++ans;
            if(x+v[i]<sum)
            {
                x+=v[i];
            }
            else
            {
                return ans;
            }
        }
        return ans;
    }
};